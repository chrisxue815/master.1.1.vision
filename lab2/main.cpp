#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

const string IMAGE_NAMES[] =
{
    "content/BabyFood-Sample0.JPG",
    "content/BabyFood-Sample1.JPG",
    "content/BabyFood-Sample2.JPG",
    "content/BabyFood-Test01.JPG",
    "content/BabyFood-Test02.JPG",
    "content/BabyFood-Test03.JPG",
    "content/BabyFood-Test04.JPG",
    "content/BabyFood-Test05.JPG",
    "content/BabyFood-Test06.JPG",
    "content/BabyFood-Test07.JPG",
    "content/BabyFood-Test08.JPG",
    "content/BabyFood-Test09.JPG",
    "content/BabyFood-Test10.JPG",
    "content/BabyFood-Test11.JPG",
    "content/BabyFood-Test12.JPG",
    "content/BabyFood-Test13.JPG",
    "content/BabyFood-Test14.JPG",
    "content/BabyFood-Test15.JPG",
    "content/BabyFood-Test16.JPG",
    "content/BabyFood-Test17.JPG",
    "content/BabyFood-Test18.JPG"
};

const int NUM_IMAGES = sizeof(IMAGE_NAMES) / sizeof(IMAGE_NAMES[0]);

inline double distance(int x1, int y1, int x2, int y2)
{
    return sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2));
}

int detectSpoon(const Mat &image)
{
    const int LEFT = 200;
    const int TOP = 100;
    const int DIAMETER = 240;

    const int RIGHT = LEFT + DIAMETER;
    const int BOTTOM = TOP + DIAMETER;
    const int CENTRE_X = LEFT + DIAMETER / 2;
    const int CENTRE_Y = TOP + DIAMETER / 2;
    const int RADIUS = DIAMETER / 2;
    const int SIZE = DIAMETER * DIAMETER;

    int count = 0;

    for (int x = LEFT; x < RIGHT; ++x)
    {
        for (int y = TOP; y < BOTTOM; ++y)
        {
            if (distance(x, y, CENTRE_X, CENTRE_Y) < RADIUS)
            {
                int index = (image.cols * y + x) * 3 + 1;
                if (image.data[index] < 50)
                {
                    ++count;
                }
            }
        }
    }

    float rate = (float)count / SIZE;
    int numSpoon = rate < 0.05 ? 0 : rate < 0.15 ? 1 : 2;

    return numSpoon;
}

int main()
{
    const string WINDOW_NAME = "Baby food can";
    const Point TEXT_POSITION = Point(30, 30);
    const int FONT_FACE = FONT_HERSHEY_SCRIPT_SIMPLEX;
    const double FONT_SCALE = 1;
    const Scalar COLOR = Scalar::all(255);

    namedWindow(WINDOW_NAME);

    for (int i = 0; i < NUM_IMAGES; i++)
    {
        Mat image = imread(IMAGE_NAMES[i]);
        int numSpoon = detectSpoon(image);

        putText(image, to_string(numSpoon), TEXT_POSITION, FONT_FACE, FONT_SCALE, COLOR);

        imshow(WINDOW_NAME, image);

        printf("%s: %d\n", IMAGE_NAMES[i].c_str(), numSpoon);

        waitKey(0);
    }

    return 0;
}
